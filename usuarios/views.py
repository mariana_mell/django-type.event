from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.messages import constants 
from django.contrib import auth
from django.urls import reverse
from django.contrib import messages
from .models import Evento
from .models import Certificado
from io import BytesIO
from django.core.files.uploadedfile import InMemoryUploadedFile
from PIL import image, imageDraw, imageFont
import sys


#escrevendo uma funcao para a visualizacao de cadastro de usuario.
def cadastro(request):
    if request.method == 'GET':
        return render(request, 'cadastro.html')
    elif request.method == 'POST':
        username = request.POST.get('username')
        email= request.POST.get('email')
        senha= request.POST.get('senha')
        confirmar_senha = request.POST.get('confirmar_senha') 

        user = User.objects.filter(username=username)

        if user.exists():
            messages.add_message(request, constants.ERROR, 'Esse usuario ja existe')
            return render(request, 'cadastro.html')

        if senha != confirmar_senha:
            messages.add_message(request, constants.ERROR, 'As senhas devem ser iguais')
            return render(request, 'cadastro.html')

        elif len(senha)<8:
            messages.add_message(request, constants.WARNING, 'A senha deve ter no minimo 8 caracteres')
            return render(request, 'cadastro.html')

        
        user= User.objects.create_user(username=username, email=email, password=senha)
        user.save()
        messages.add_message(request, constants.SUCCESS, 'Usuario salvo com sucesso!')
        return redirect(reverse('login'))


#escrevendo funcao para fazer login do usuario.
@login_required
def login(request):
    if request.method == 'GET':
        return render(request, 'login.html')
    elif request.method == 'POST':
        username= request.POST.get('username')
        senha= request.POST.get('senha') 

        if len(senha)<8:
            messages.add_message(request, constants.WARNING, 'A senha deve ter no minimo 8 caracteres')
            return redirect('login')

        user= auth.authenticate(username=username, password=senha)
        
        if not user:
            messages.add_message(request, constants.ERROR, 'Username ou senha invalidos')
            return redirect('login')

        auth.login(request, user)
        return redirect('/eventos/novo_evento')


#escrevendo funcao para salvar o evento
@login_required
def novo_evento(request):
    if request.method == 'GET':
        return render(request, 'novo_evento.html')
    elif request.method == 'POST':
        nome = request.POST.get('nome')
        descricao = request.POST.get('descricao')
        data_inicio = request.POST.get('data_inicio') 
        data_termino = request.POST.get('data_termino') 
        carga_horaria = request.POST.get('carga_horaria') 

        logo = request.FILES.get('logo')

        novo_evento = Evento(criador=request.user, nome=nome, descricao=descricao, 
        data_inicio=data_inicio, data_termino=data_termino, carga_horaria=carga_horaria, logo=logo,)

        novo_evento.save()

        messages.add_message(request, constants.SUCCESS, 'Evento criado com sucesso')
        return redirect(reverse('listar_eventos'))


#escrevendo funcao para gerenciar evento
@login_required
def gerenciar_evento(request):
    if request.method == GET:
        nome = request.POST.get('nome')
        eventos = Evento.objects.filter(criador=request.user)
        if nome:
            eventos = eventos.filter(nome__contains=nome)

        return render(request, 'gerenciar_evento.html', {'eventos':eventos})


#escrevendo funcao para inscrever evento
@login_required
def inscrever_evento(request, id):
    evento = get_object_or_404(Evento, id=id)
    if request.method == 'GET':
        return render(request, 'inscrever_evento.html', {'evento':evento})
    elif request.method == 'POST':
        if not request.user.is_authenticated:
            return redirect('login')
        if evento.participantes.filter(id=request.user.id).exists():
            messages.add_message(request, constants.ERROR, 'Voce ja esta inscrito nesse evento.')
        else:
            evento.participantes.add(request.user)
            evento.save()
            messages.add_message(request, constants.SUCCESS, 'Inscrição realizada.')

        return redirect(reverse('increver_evento', kwargs={'id':id})) 

#escrevendo funcao para lista de participantes do evento
@login_required
def participantes_evento(request, id):
    evento = get_object_or_404(Evento, id=id)
    if not evento.criador == request.user:
        raise Http404('Voce nao esta inscrito nesse evento.')
    if request.method == 'GET':
        participantes = evento.participantes.all()[::3]
        return render(request, 'participantes_evento.html', {'evento':evento, 'participantes':participantes})


#criando funcao para gerar csv do participante
def gerar_csv(request, id):
    evento = get_object_or_404(Evento, id=id)
    if not evento.criador == request.user:
        raise Http404('Voce nao esta inscrito nesse evento.')
    participantes = evento.participantes.all()

    token = f'{token_urlsafe(6)}.csv'
    path= os.path.join(settings.MEDIA_ROOT, token)

    with open(path, 'w') as arq:
        writer = csv.writer(arq, delimiter=",")
        for participante in participantes:
            x = (participante.username, participante.email)
            writer.writerow(x)

    return redirect(f'/media/{token}')


#criando funcao para gerar a quantidade de certificados para o usuario
def certificados_evento(request, id):
    evento = get_object_or_404(Evento, id=id)
    if not evento.criador == request.user.is_authenticated:
        raise Http404('Voce nao e o criador do evento')
    if request.method == 'GET':
        total_certificados = evento.participantes.all().count() - Certificado.objects.filter(evento=evento).count()
        return render(request, 'certificados_evento.html', {'evento':evento, 'total_certificados':total_certificados})


#criando funcao para gerar certificado
def gerar_certificado(request, id):
    evento = get_object_or_404(Evento, id=id)
    if not evento.criado == request.user:
        raise Http404('Esse evento nao e seu')

    path_template = os.path.join(settings.BASE_DIR, 'templates/static/evento/img/template_certificado.png')
    path_fonte = os.path.join(settings.BSE_DIR, 'templates/static/fontes/arimo.ttf')
    for participante in evento.participantes.all():
        #todo:validar se ja existe certificado desse participante para esse evento
        img = Image.open(path_template)
        path_template = os.path.join(settings.BASE_DIR, 'templates/static/evento/img/template_certificado.png')
        draw = ImageDraw.Draw(img)
        fonte_nome = ImageFont.truetype(path_fonte, 60)
        fonte_info =  ImageFont.truetype(path_fonte, 30)
        draw.text((230, 651), f"{participante.username}", font=fonte_nome, fill=(0, 0, 0))
        draw.text((761, 782), f"{evento.nome}", font=fonte_info, fill=(0, 0, 0))
        draw.text((816, 849), f"{evento.carga_horaria} horas", font=fonte_info, fill=(0, 0, 0))
        output = BytesIO()
        img.save(output, format="PNG", quality=100)
        output.seek(0)
        img_final = InMemoryUploadedFile(output, 'ImageField',f'{token_urlsafe(8)}.png' 'image/jpeg', sys.getsizeof(output), None)

        certificado_gerado = Certificado(certificado=img_final, participante=participante, evento=evento, )
        message.add_message(request, constants.SUCCESS, 'Certificados gerados')
        return redirect(reverse('certificados_evento', kwargs={'id': evento.id}))

#criando funcao para procurar certificado
def procurar_certificado(request, id):
    evento = get_object_or_404(Evento, id=id)
    if not evento.criador == request.user:
        raise Http404('Esse evento não é seu')
    email = request.POST.get('email')
    if not email == evento.criador.email:
        messages.add_message(request, constants.ERROR, 'Esse email nao e valido')
        return redirect(reverse('certificados_evento', kwargs={'id': evento.id}))
    certificado = Certificado.objects.filter(evento=evento).filter(participante__email=email).first()
    if not certificado:
        messages.add_message(request, constants.WARNING, 'Certificado não encontrado')
        return redirect(reverse('certificados_evento', kwargs={'id': evento.id}))

    return redirect(certificado.certificado.url)

#criando funcao para visualizar certificado
def meus_certificados(request):
    certificados = Certificado.object.filter(participante=request.user)
    return render(request, 'meus_certificados.html', {'certificados':certificados})